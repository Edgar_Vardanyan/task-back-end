const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require('passport');
const app = express();

const {passportMiddleware} = require('./app/middlewares/index');
passportMiddleware(passport);

var corsOptions = {
    origin: "http://localhost:3000/"
};

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());

require('./app/db/mongoseConnection')

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to Task" });
});


require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app,passport);
require('./app/routes/workspace.routes')(app,passport);

// set port, listen for requests
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});