const controller = require("../controllers/workspace.controller");
const validator = require('../validation');

module.exports = function(app,passport) {

    app.post("/api/workspace/create",
        [
            passport.authenticate('jwt', { session: false}),
            validator.workspaceValidator.workspaceValidator()
        ],
        controller.create);

    app.get("/api/workspace/list",passport.authenticate('jwt', { session: false}),controller.list)

    app.put("/api/workspace/edit",
        [
            passport.authenticate('jwt', { session: false}),
            validator.workspaceValidator.workspaceValidator()
        ],
        controller.edit)

    app.delete("/api/workspace/delete/:id",passport.authenticate('jwt', { session: false}),controller.delete)

    app.get("/api/workspace/:id",passport.authenticate('jwt', { session: false}),controller.workspace)

    app.post("/api/workspace/find",passport.authenticate('jwt', { session: false}),controller.find)

};
