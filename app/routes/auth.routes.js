const controller = require("../controllers/auth.controller");
const validator = require('../validation');

module.exports = function(app) {

    app.post("/api/auth/signup",
        validator.userValidator.registerValidator(),
        controller.signup
    );

    app.post("/api/auth/signin",
        validator.userValidator.loginValidator() ,
    controller.signin);

};