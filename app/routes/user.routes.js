const controller = require("../controllers/user.controller");

module.exports = function(app,passport) {

    app.get("/api/auth/user", [passport.authenticate('jwt', { session: false})], controller.user);

};