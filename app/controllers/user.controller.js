exports.user = (req, res) => {
    let user = req.user;
    if (user) {
        res.json({
            success: true,
            user: {
                username: user.username,
                email: user.email,
            }
        });
    } else res.status(401).send({success: false, errors: "User not found"});
};