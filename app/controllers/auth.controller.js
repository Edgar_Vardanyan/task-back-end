exports = module.exports = {};
const {expressValidatorResult} = require('../validation');

let config = require('../config/auth.config'),
    jwt = require('jsonwebtoken');

// Call User model
let User = require("../models/user.model");

exports.signup = function(req, res) {

    // request validation
    const errors = expressValidatorResult(req);

    if (errors) {
        return res.status(400).json({
            success: false,
            errors
        });
    }

    let newUser = new User({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    });

    // save the user
    newUser.save(function(err) {
        if (err) {
            // Duplicate User
            return res.status(400).send({ success: false , errors: { form:"There is already a user with this data" } });
        }
        res.json({success: true, message: 'Successful created new user.'});
    });

};

exports.signin = function(req, res) {

    // request validation
    const errors = expressValidatorResult(req);

    if (errors) {
        return res.status(400).json({
            success: false,
            errors
        });
    }

    User.findOne({
        username: req.body.username
    }, function(err, user) {
        // if (err) throw err;
        if (!user) {
            res.status(401).send({success: false, errors:{ form: "Authentication failed. User not found."}});
        } else {
            // check if password matches
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    let token = jwt.sign(user.toJSON(), config.secret);
                    // return the information including token as JSON
                    res.json({
                        success: true,
                        message: "You have successfully logged in․",
                        user:{
                            username: user.username,
                            email: user.email,
                            token: token
                        }
                    });
                } else {
                    res.status(401).send({success: false, errors: {password:"Authentication failed. Wrong password."}});
                }
            });
        }
    });
};