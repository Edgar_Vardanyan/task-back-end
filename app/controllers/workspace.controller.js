exports = module.exports = {};
const {expressValidatorResult} = require('../validation');

// Call Workspace model
let WorkspaceModel = require("../models/workspace.model");

exports.create = (req, res) => {
    // request validation
    const errors = expressValidatorResult(req);

    if (errors) {
        return res.status(400).json({
            success: false,
            errors
        });
    }

    let newWorkspace = new WorkspaceModel({
        name: req.body.name,
        description: req.body.description,
        type: req.body.type,
        user: req.user,
    });

    // save the user
    newWorkspace.save(function (err) {
        if (err) {
            // Duplicate User
            return res.status(400).send({success: false, errors: {name: "That name is already taken"}});
        }

        res.json({success: true, message: 'Successful created new Workspace.'});
    });

};

exports.edit = async (req, res) => {

    // request validation
    const errors = expressValidatorResult(req);

    if (errors) {
        return res.status(400).json({
            success: false,
            errors
        });
    }
    const {id,name,description,type} = req.body;

    const duplicated = await WorkspaceModel.findOne({name,_id: { $ne: id }})
    if (duplicated && duplicated._id){
        res.status(400).send({success: false, errors: { name:"That name is already taken"}})
    }else{
        const updated = await WorkspaceModel.findByIdAndUpdate(id, {
            name,description,type
        })

        // console.log(updated)
        if (updated && updated._id) {
            res.status(200).send({success: true, message: "Successfully updated"})
        } else {
            res.status(400).send({success: false, errors: {form:"Something is Wrong!"}})
        }
    }

};

exports.list = async (req, res) => {

    let workspaces = await WorkspaceModel.find({ user : req.user._id});

    if (workspaces) {
        res.status(200).send({success: true, data: workspaces})
    } else {
        res.status(401).send({success: false, message: "Something is Wrong!"});
    }
};

exports.delete = async (req, res) => {
    const {id} = req.params;
    const deleted = await WorkspaceModel.findByIdAndDelete(id);

    if (deleted && deleted._id) {
        res.status(200).send({success: true, message: "Successfully deleted"})
    } else {
        res.status(401).send({success: false, message: "Something is Wrong!"});
    }
};

exports.workspace = async (req, res) => {
    const {id} = req.params;
    const workspace = await WorkspaceModel.findById(id);
    if (workspace && workspace._id) {
        res.status(200).send({success: true, data: workspace})
    } else {
        res.status(401).send({success: false, message: "Something is Wrong!"});
    }
};

exports.find = async (req, res) => {
    const {name} = req.body;
    const regex = new RegExp(`^${name}`);
    const workspace = await WorkspaceModel.find({
        name: {$regex: regex}
    });

    if (workspace && workspace.length) {
        res.status(200).send({success: true, data: workspace})
    } else {
        res.status(401).send({success: false, message: "Something is Wrong!"});
    }
};