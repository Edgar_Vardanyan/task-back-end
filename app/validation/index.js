const userValidator = require("./user.validator");
const workspaceValidator = require("./workspace.validator");
const {validationResult} = require('express-validator');


const expressValidatorResult = (req) => {
    const errors = validationResult(req)
    let messages = null
    if (!errors.isEmpty()) {
        messages = {}
        for (const err of errors.array()) {
            messages[err.param] = err.msg
        }
    }
    return messages
}

module.exports = {
    expressValidatorResult,
    userValidator,
    workspaceValidator,
};


