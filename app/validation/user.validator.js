const {check} = require('express-validator');

const registerValidator = () => [
    check('username').trim()
        .notEmpty().withMessage('Username is required').bail()
        .isLength({ min: 3 }).withMessage('Username must be at least 8 characters'),

    check('email').trim()
        .notEmpty().withMessage('Email is required').bail()
        .isEmail().withMessage('Email is not valid'),

    check('password').trim()
        .notEmpty().withMessage('Password is required').bail()
        .isLength({ min: 8 }).withMessage('Password must be at least 8 characters'),

    check('passwordConfirmation').trim()
        .not()
        .custom((value, { req }) => req.body.password !== value)
        .withMessage('Confirm password is not match with password').bail()
        .notEmpty().withMessage('Password confirmation is required')
]

const loginValidator = () => [
    check('username').trim()
        .notEmpty().withMessage('Username is required'),
    check('password').trim().notEmpty().withMessage('Password is required')
]

module.exports = {
    registerValidator,
    loginValidator
}