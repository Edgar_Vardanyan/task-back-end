const {check} = require('express-validator');

const workspaceValidator = () => [
    check('name').trim()
        .notEmpty().withMessage('Workspace name is required').bail()
        .isLength({ min: 3 }).withMessage('Name must be at least 2 characters․'),

    check('description').trim()
        .isLength({ max:90 }).withMessage("The description must be not less than 90․"),
]


module.exports = {
    workspaceValidator
}