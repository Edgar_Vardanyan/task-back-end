var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WorkspaceSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description:{
        type: String,
    },
    type:{
        type: String ,default: 'Private'
    },
    user : { type: mongoose.Schema.Types.ObjectId,ref:'User'}

});

module.exports = mongoose.model('Workspace', WorkspaceSchema);